//I/O Stream
#include <iostream>
#include <string>
#include "ros/ros.h"

//trajectory prediction
#include "aukf_tra_pre.h"

int main(int argc, char **argv)
{
  //Init
  ros::init(argc, argv, "aukf_tra_pre_node"); //Say to ROS the name of the node and the parameters
  double frequency;
  ros::param::get("~frequency", frequency);
  if(frequency == 0)
    frequency = 100;
  aukf_tra_pre aukf_tra_pre_process;
  //Open!
  aukf_tra_pre_process.setUp();
  ros::Rate r(frequency);
  //Loop -> Ashyncronous Module
  while(ros::ok())
  {
    ros::spinOnce();
    aukf_tra_pre_process.run();
    r.sleep();
  }
  return 1;
}
