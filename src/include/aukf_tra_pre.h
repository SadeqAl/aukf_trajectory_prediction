#ifndef aukf_tra_pre_H
#define aukf_tra_pre_H

//STL
#include <ctime>
#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include <numeric>
#include <stdio.h>
#include <fstream>
#include <vector>
#include <iostream>
#include <string>
#include "cstdlib"

//egien
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Eigen>
#include <eigen3/Eigen/Core>

//opencv
#include <opencv/cv.h>
#include <opencv/cv.hpp>
#include <opencv/highgui.h>
#include "opencv2/core/core.hpp"

//ROS
#include "ros/ros.h"

//Drone module
#include "robot_process.h"

//ROS message
#include "nav_msgs/Path.h"
#include "visualization_msgs/Marker.h"
#include "visualization_msgs/MarkerArray.h"
#include "geometry_msgs/Pose.h"
#include "std_msgs/String.h"
#include <geometry_msgs/PointStamped.h>

//DroneMsgsROS
#include <droneMsgsROS/dronePose.h>
#include <droneMsgsROS/droneSpeeds.h>
#include <droneMsgsROS/droneTrajectoryRefCommand.h>
#include <droneMsgsROS/droneRefCommand.h>
#include <droneMsgsROS/dronePositionTrajectoryRefCommand.h>
#include <droneMsgsROS/dronePositionRefCommandStamped.h>
#include <droneMsgsROS/droneCommand.h>
#include <droneMsgsROS/droneYawRefCommand.h>
#include <droneMsgsROS/robotPoseStampedVector.h>
#include <droneMsgsROS/robotPoseStamped.h>

//Gazebo messages
#include "gazebo_msgs/ModelStates.h"
#include "gazebo_msgs/ModelState.h"
#include "gazebo_msgs/SetModelState.h"

#include "aukf.h"

class aukf_tra_pre : public RobotProcess
{
public:
  aukf_tra_pre();
  ~aukf_tra_pre();

  void ownSetUp();
  void ownStart();
  void ownStop();
  void ownRun();
  void init();
  void drone_pose_callback(const gazebo_msgs::ModelStates::ConstPtr &msg);
  void predicted_pose_callback(const droneMsgsROS::robotPoseStampedVector &msg);
  void updated_pose_callback(const droneMsgsROS::dronePose &msg);
  void display_mav_tra(cv::Point3d mav_pose);
  void display_pre_tra(cv::Point3d mav_pose);
  void display_upd_tra(cv::Point3d mav_pose);
  void TrajPrediction(Eigen::VectorXd &X_Pre, Eigen::MatrixXd &P_Pre, Eigen::MatrixXd &F_Pre, double deltaT,
                       std::vector<cv::Point3d> &predicted_vector, int limit_number);

private:
  struct {
    ros::Time stamp_;
    double x, y, z;
  }measurements_;

  std::string stack_path_;
  std::string config_file_;
  std::string drone_id_;
  std::string drone_predicted_pose_;
  std::string drone_updated_pose_;
  std::string Lemniscate_tra_;
  std::string predicted_tra_;
  std::string updated_tra_;
  ros::NodeHandle n_;
  Eigen::VectorXd X_aukf_ = Eigen::VectorXd(13);
  Eigen::VectorXd X_Measure = Eigen::VectorXd(13);
  Eigen::MatrixXd P_ = Eigen::MatrixXd(13,13);
  Eigen::MatrixXd F_ = Eigen::MatrixXd(13,13);

  int limit_number_;
  bool received_odom_data_, new_updated_pose_;
  double deltaT_, timePrev_, timeNow_;

  ros::Subscriber Lemniscate_tra_sub_, pre_tra_sub_, upd_tra_sub_;
  ros::Publisher Lemniscate_tra_pub_, predicted_tra_pub_,
  updated_tra_pub_, drone_pre_tra_pub_, drone_upd_tra_pub_;
  visualization_msgs::Marker mav_marker_, pre_marker_, upd_marker_;
};

#endif
